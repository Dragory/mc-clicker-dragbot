import io from 'socket.io-client';
import config from '../config.json';

import ChatBot from './ChatBot';

import testCommand from './commands/test';
import eightBallCommand from './commands/eightBall';
import gradientCommand from './commands/gradient';
import slapCommand from './commands/slap';
import sendHelpCommand from './commands/sendHelp';
import swearCommand from './commands/swear';
import numCommand from './commands/num';
import helpCommand from './commands/help';
import hugCommand from './commands/hug';
import relayCommand from './commands/relay';
import mcchatbotResponsesCommand from './commands/mcchatbotResponses';

let client = io.connect('http://5.189.137.117:469');
console.log('connecting');

function startBot() {
	let chatBot = new ChatBot(client);

	chatBot.addCmd(testCommand);
	chatBot.addCmd(eightBallCommand);
	chatBot.addCmd(gradientCommand);
	chatBot.addCmd(slapCommand);
	chatBot.addCmd(sendHelpCommand);
	// chatBot.addCmd(swearCommand); // Commented out because Rune is a nazi
	chatBot.addCmd(numCommand);
	chatBot.addCmd(helpCommand);
	chatBot.addCmd(hugCommand);
	chatBot.addCmd(relayCommand);
	chatBot.addCmd(mcchatbotResponsesCommand);
}

client.on('connect', function() {
	console.log('connected, logging in');
	client.emit('chatLogIn', {
		username: config.username,
		password: config.password
	});
});

client.on('loggedIn', function(result) {
	console.log('login result', result);
	if (result) {
		console.log('Logged in');
		startBot();
	} else {
		console.error('Login failed!');
	}
});
