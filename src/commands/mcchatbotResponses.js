import config from '../../config.json';

let mcchatbotResponsesCommand = {
	accept: function(msg) {
		return (msg.username.toLowerCase() === 'mcchatbot');
	},

	run: function(msg, client, reply) {
		if (msg.message === "Chat Breakers is the best!") {
			reply("Beats the rest!");
		}

		if (msg.message === "pylon has many members!") {
			reply("too many in fact...");
		}

		if (msg.message === "ccm are recruiting!") {
			reply("yeah, bots");
		}

		if (msg.message === "clickclickmafia are a bunch of bots!") {
			reply("maybe I should ask to join?");
		}

		if (msg.message === "Now You Try It!") {
			reply("You can also use a background wizard by typing +grad");
		}
	}
};

export default mcchatbotResponsesCommand;
