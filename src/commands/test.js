let testCommand = {
	accept: function(msg) {
		return (msg.message.match(/^test$/i) !== null);
	},

	run: function(msg, client, reply) {
		reply('test yourself, ' + msg.username);
	}
};

export default testCommand;
