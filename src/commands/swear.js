let swears = [
	'bullshit',
	'fucking',
	'fucker',
	'fuck',
	'shit',
	'cunt'
];

swears = swears.map(function(swear) {
	let regex = '';
	for (let i = 0; i < swear.length; i++) {
		regex += '(' + swear[i] + '|\\*)';
	}
	return {word: swear, regex: RegExp(regex, 'i'), textRegex: regex};
});

function uncensor(text) {
	let corrections = [],
	    tempText = text;

	swears.forEach(function(swear) {
		let pos = 0,
		    handled = [];

		while (pos < tempText.length) {
			let innerTempText = tempText.slice(pos);

			let match = innerTempText.match(swear.regex);
			if (match === null) break;
			if (match[0].indexOf('*') === -1) return;

			let prevWhitespaceIndex = (innerTempText.slice(0, match.index).split('').reverse().join('').match(/\s|$/) || {index: 0}).index;
			let fullWord = innerTempText.slice(match.index - prevWhitespaceIndex).match(/^[^\s]+/)[0];

			corrections.push([match.index, match.index + fullWord.length, swear]);
			handled.push([match.index, match.index + match[0].length]);

			pos += match.index + match[0].length;
		}

		// Remove the parts/words we handled for this swear
		handled.forEach(function([start, end]) {
			tempText = tempText.slice(0, start) + tempText.slice(end);
		});
	});

	corrections.forEach(function([start, end, swear]) {
		var regex = new RegExp(swear.textRegex, 'ig');
		text = text.replace(regex, swear.word);
	});

	return text;
}

let swearCommand = {
	accept: function(msg) {
		return true;
	},

	run: function(msg, client, reply) {
		let correctedText = uncensor(msg.message);
		if (correctedText != msg.message) reply(`Did you mean: ${correctedText}`);
	}
};

export default swearCommand;
