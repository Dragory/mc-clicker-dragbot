import config from '../../config.json';

let slapCommand = {
	accept: function(msg) {
		return (msg.message.match(/^\+slap/i) !== null || msg.message.match(/\*slaps ([^\*]+)\*/i) !== null);
	},

	run: function(msg, client, reply) {
		let toSlap = null;
		let slapMatch;
		
		slapMatch = msg.message.match(/^\+slap (.+)$/i);
		if (slapMatch !== null) {
			toSlap = slapMatch[1];
		}
		
		slapMatch = msg.message.match(/\*slaps ([^\*]+)\*/i);
		if (slapMatch !== null) {
			toSlap = slapMatch[1];
		}

		if (! toSlap) {
			reply("I don't know who to slap! (Try adding a name)");
			return;
		}
		
		if (msg.username === 'NewChatBot' && toSlap.toLowerCase() === 'dragbot') toSlap = 'NewChatBot';

		if (toSlap.toLowerCase() === config.username.toLowerCase()) {
			reply('ow :(');
		} else if (toSlap.toLowerCase() === msg.username.toLowerCase() || toSlap.toLowerCase() === 'himself'  || toSlap.toLowerCase() === 'herself' || toSlap.toLowerCase() === 'themselves') {
			reply(' ' + msg.username + ' slaps themselves! Stop hitting yourself!');
		} else {
			reply(' ' + msg.username + ' slaps ' + toSlap);
		}
	}
};

export default slapCommand;
