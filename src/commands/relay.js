let relayCommand = {
	accept: function(msg) {
		return (msg.message.match(/^\+relay (.*)/i) !== null);
	},

	run: function(msg, client, reply) {
		if (msg.username.toLowerCase() !== 'dragory') return;

		let text = msg.message.match(/^\+relay (.*)/i)[1];
		if (! text) return;

		let groupMsg = (text.match(/^\+group (.*)/i) || [])[1];
		if (groupMsg) {
			console.log("Relaying group message");
			reply(groupMsg, 'group');
		} else {
			console.log("Relaying general message");
			reply(text, 'general');
		}
	}
};

export default relayCommand;
