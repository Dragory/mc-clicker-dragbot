let answers = [
	"It is certain",
	"It is decidedly so",
	"Without a doubt",
	"Yes definitely",
	"You may rely on it",
	"As I see it, yes",
	"Most likely",
	"Outlook good",
	"Yes",
	"Signs point to yes",
	"Don't count on it",
	"My reply is no",
	"My sources say no",
	"Outlook not so good",
	"Very doubtful",
	"If you ask me, yes; lanza would disagree"
];

let eightBallCommand = {
	accept: function(msg) {
		return (msg.message.match(/^\+8ball/i) !== null);
	},

	run: function(msg, client, reply) {
		reply(answers[Math.floor(Math.random() * answers.length)]);
	}
};

export default eightBallCommand;
