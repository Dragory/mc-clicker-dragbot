import config from '../../config.json';

let magnitudeSymbols = {
	0: '',
	1: 'K',
	2: 'M',
	3: 'B',
	4: 'T',
	5: 'Qa',
	6: 'Qi',
	7: 'Sx',
	8: 'Sp',
	9: 'Oc',
	10: 'No',
	11: 'Dc',
	12: 'UDc'
};

function formatNum(num, decimalCount){
	var magnitude, rounded, decimals, symbol;
	if (typeof decimalCount !== 'Number') decimalCount = 2;

	// Remove commas and truncate pre-existing decimals
	num = num.toString().replace(/[\s,]/g, '').replace(/\.[0-9]+/, '');

	// Figure out the magnitude from the number string length (minus one because 999 999 isn't a million yet)
	magnitude = Math.floor((num.length - 1) / 3);
	while (! magnitudeSymbols[magnitude] && magnitude > 0) magnitude--;

	// "Round" by removing the smaller numbers, leaving only one "decimal"
	// The fallbacks to 1000 are failsafes for numbers < 1000
	rounded = num.slice(0, (-3 * magnitude || 1000));
	decimals = num.substr((-3 * magnitude || 1000), decimalCount);
	if (decimals.match(/^0+$/) === null && decimals !== '') rounded += '.' + decimals;

	symbol = magnitudeSymbols[magnitude];

	return rounded.toString() + symbol;
}

let numCommand = {
	accept: function(msg) {
		return (msg.message.match(/^\+num/i) !== null);
	},

	run: function(msg, client, reply) {
		let num = (msg.message.match(/^\+num ([0-9,\. ]+)/) || [])[1];
		console.log('num', num);
		if (! num) return;

		let formatted = formatNum(num, 2);

		reply(formatted);
	}
};

export default numCommand;
