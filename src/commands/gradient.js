let status = 0,
    gradUser = null,
    gradType,
    gradDir;

class Generator {
	constructor(username) {
		this.status = 0;

		this.gradType = null;
		this.gradDir = null;
		this.username = username;
		this.colors = null;
	}

	setStatus(newStatus) {
		this.status = newStatus;
	}

	next(msg, client, reply) {
		let lowerMsg = msg.message.toLowerCase();

		if (msg.username !== this.username) return;

		if (lowerMsg === 'nvm' || lowerMsg === 'nevermind' || lowerMsg === 'cancel' || lowerMsg === 'quit') {
			reply('Ok');
			return true;
		}

		if (this.status === 0) {
			reply('Linear or radial?');
			this.setStatus(1);
			
			return;
		}

		if (this.status === 1) {
			if (lowerMsg === 'linear') {
				this.gradType = 'linear';
				this.setStatus(2);
				reply('Ok, a linear gradient. Which direction? (e.g. right, left, top, 87, 170)');
			}

			if (lowerMsg === 'radial') {
				this.gradType = 'radial';
				this.setStatus(3);
				reply('Ok, a radial gradient. Which colors? (separate with commas, feel free to add percentages)');
			}

			return;
		}

		if (this.status === 2) {
			if (lowerMsg.match(/^right|left|top|bottom$/)) {
				this.gradDir = 'to\t' + lowerMsg;
			} else if (lowerMsg.match(/^[0-9]+(deg)?$/)) {
				let deg = (lowerMsg.match(/^[0-9]+/) || [])[0] || '0';
				this.gradDir = deg + 'deg';
			} else {
				return;
			}

			this.setStatus(3);
			reply('Ok, ' + this.gradDir + '. And which colors? (separate with commas, feel free to add percentages)');
			return;
		}

		if (this.status === 3) {
			this.colors = lowerMsg;
			this.setStatus(4);
			reply('Ok. And finally, name color?');

			return;
		}

		if (this.status === 4) {
			let gradient = '';

			if (this.gradType === 'linear') {
				gradient = `linear-gradient(${this.gradDir}, ${this.colors})`;
			} else if (this.gradType === 'radial') {
				gradient = `radial-gradient(${this.colors})`;
			}

			gradient = gradient.replace(/\s+/g, '\t');
			reply('Done! Use this to set your color: /color ' + gradient + ' ' + msg.message);

			return true;
		}
	}
}

let pendingGenerators = {};

let gradientCommand = {
	accept: function(msg) {
		return true;
	},

	run: function(msg, client, originalReply) {
		let reply = (text) => {
			originalReply(text, 'pm');
		};

		if (msg.message.toLowerCase() === '!help color') {
			client.emit('general_chat_message', 'Want a gradient background? Type +grad');
		}

		if (msg.message.match(/^\+grad(ient)?/i) !== null) {
			pendingGenerators[msg.username] = (new Generator(msg.username));
		}

		// Run each generator's "next" method
		// The generator is removed when it returns true.
		for (let prop in pendingGenerators) {
			let isFinished = pendingGenerators[prop].next(msg, client, reply);
			if (isFinished) delete pendingGenerators[prop];
		}
	}
};

export default gradientCommand;
