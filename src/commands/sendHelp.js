import config from '../../config.json';

let sendHelpCommand = {
	accept: function(msg) {
		return (msg.message.match(/^\+sendhelp/i) !== null);
	},

	run: function(msg, client) {
		// Only allow using this command via a PM
		if (! msg.to) return;

		let parts = msg.message.split(/\s+/),
		    target = parts.slice(1, 2),
		    command = parts.slice(2).join(' ').toLowerCase();

		let sendHelp = (text) => {
			client.emit('private_chat_message', {
				to: target,
				message: text
			});
		};

		if (command === 'attack') {
			sendHelp(`The attack tab now only shows up when you have something to attack. Thank the devs!`);
		}

		if (['dragory', 'runedaegun', 'lanza', 'tjmonk15', 'developer'].indexOf(msg.username.toLowerCase()) !== -1) {
			if (command === 'color') {
				sendHelp(`If you want to change your chat color the command is "/color bgcolor textcolor", so a black background with white text would be "/color black white". You can use hex codes or you can find a list of color names here http://www.javascripter.net/faq/colornam.htm (also note that /color rainbow textcolor works as well)`);
			}

			if (command === 'tutorial') {
				sendHelp(`See the following link for a basic tutorial http://minecraftclicker.boards.net/thread/18/tutorial-faq`);
			}
		}
	}
};

export default sendHelpCommand;
