import config from '../../config.json';

let hugCommand = {
	accept: function(msg) {
		return (msg.message.match(/^\+hug (.*)/i) !== null);
	},

	run: function(msg, client, reply) {
		let target = msg.message.match(/^\+hug (.*)/i)[1].trim();
		if (target == '') return;

		if (target.toLowerCase() === config.username.toLowerCase()) {
			reply(`aww thanks <3`, 'general');
		} else {
			reply(` ${msg.username} hugs ${target} :)`, 'general');
		}
	}
};

export default hugCommand;
