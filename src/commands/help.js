let helpCommand = {
	accept: function(msg) {
		return (msg.message.match(/^\+help/i) !== null);
	},

	run: function(msg, client, reply) {
		reply(`.

	My commands are:
	
		+grad	(create a name background interactively)
		+8ball	(to tell the truth!)
		+num	(shorten big numbers)
		+slap
		+hug
	
	To use these, type them in chat!
		.`, 'pm');
	}
};

export default helpCommand;
