import config from '../config.json';

let spamCounts = {};

setInterval(() => {
	for (let user in spamCounts) {
		if (spamCounts[user] === 0) continue;
		spamCounts[user] -= 1;
	}
}, 2000);

function isSpamming(user) {
	return (spamCounts[user] > 2);
}

class ChatBot {
	commands = [];

	constructor(client) {
		this.client = client;

		this.client.on('general_chat_message', (msg) => {
			this.onMessage(msg);
		});

		this.client.on('private_chat_message', (msg, _1, _2) => {
			msg.to = config.username;
			this.onMessage(msg);
		});

		this.client.on('group_chat_message', (msg) => {
			msg.group = true;
			this.onMessage(msg);
		});

		this.client.on('kicked', function() {
			console.log('KICKED! :(');
			console.log([].slice.call(arguments));
		});

		this.client.on('disconnect', function() {
			console.log('DC! D:');
			console.log([].slice.call(arguments));
		});
	}

	addJob(job) {
		job.run(this.client);
	}

	addCmd(cmd) {
		this.commands.push(cmd);
	}

	onMessage(msg) {
		if (msg.username.toLowerCase() === config.username.toLowerCase()) return;

		let reply = (text, type) => {
			type = type || 'auto';

			if (type === 'pm' || (type === 'auto' && msg.to)) {
				this.client.emit('private_chat_message', {to: msg.username, message: text});
			} else if (type === 'group' || (type === 'auto' && msg.group)) {
				this.client.emit('group_chat_message', text);
			} else {
				this.client.emit('general_chat_message', text);
			}
		}

		if (msg.message.substr(0, 1) === '+') {
			spamCounts[msg.username] = spamCounts[msg.username] || 0;
			spamCounts[msg.username] += 1;

			if (isSpamming(msg.username)) {
				spamCounts[msg.username] += 3; // Timed OUT bitch
				reply('Stop spamming (try again in ~' + (spamCounts[msg.username] * 2) + ' seconds)', 'pm');
				return;
			}
		}

		this.commands.forEach((cmd) => {
			if (cmd.accept(msg)) {
				cmd.run(msg, this.client, reply);
			}
		});
	}
}

export default ChatBot;
